package com.xatalk.assistant;

import android.content.Context;

import org.greenrobot.greendao.query.WhereCondition;

import java.util.List;

/**
 * Created by Administrator on 2018/3/13 0013.
 * 自动回复 关键词回复
 * 自动清人 邀请到群
 * 定时发送文章
 */

public class ReplyKeyDataUtils{
    private Context context;
    private ReplyKeyBeanDao replyKeyBeanDao;

    public ReplyKeyDataUtils(Context context) {
        this.context = context;
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(context, "auto_reply_data.db", null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
        DaoSession daoSession = daoMaster.newSession();
        replyKeyBeanDao = daoSession.getReplyKeyBeanDao();
    }
    public ReplyKeyBeanDao getReplyKeyBeanDao(){
        return replyKeyBeanDao;
    }
    public void insertData(ReplyKeyBean replyKeyBean) {
        replyKeyBeanDao.insert(replyKeyBean);
    }

    public void deleteData(String key) {
        replyKeyBeanDao.deleteByKey(key);//通过 Id 来删除数据
    }

    public void updateData(ReplyKeyBean replyKeyBean) {
        replyKeyBeanDao.update(replyKeyBean);
    }

    public List<ReplyKeyBean> getReplyData(String key) {

      /*  List<ReplyKeyBean> list = replyKeyBeanDao.queryBuilder()
                .offset(1)//偏移量，相当于 SQL 语句中的 skip
                .limit(3)//只获取结果集的前 3 个数据
                .orderAsc(ReplyKeyBeanDao.Properties.Id)//通过 StudentNum 这个属性进行正序排序
                .where(ReplyKeyBeanDao.Properties.Id.eq("zone"))//数据筛选，只获取 Name = "zone" 的数据。
                .build()
                .list();*/
        List<ReplyKeyBean> list = replyKeyBeanDao.queryBuilder()
                .orderAsc(ReplyKeyBeanDao.Properties.Id)//通过 StudentNum 这个属性进行正序排序
                .where(ReplyKeyBeanDao.Properties.Key.eq(key))//数据筛选，只获取 Name = "zone" 的数据。
                .build()
                .list();
        return list;
    }
}
