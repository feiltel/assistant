package com.xatalk.assistant;

import android.accessibilityservice.AccessibilityService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Administrator on 2018/3/16 0016.
 */

public class AddFriendUtils {
    private Context context;
    private AccessibilityEvent event;
    private AccessibilityService accessibilityService;


    public AddFriendUtils(Context context, AccessibilityEvent event, AccessibilityService accessibilityService) {
        this.context = context;
        this.accessibilityService = accessibilityService;
        this.event = event;
        onCreate();
    }

    private void onCreate() {
        int eventType = event.getEventType();
        switch (eventType) {
            case AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED:
                onNotificationChange(event);
                break;
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                Toast.makeText(context, ">>>>>>>>>>>", Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void onNotificationChange(AccessibilityEvent event) {
        List<CharSequence> textList = event.getText();
        if (!textList.isEmpty()) {
            for (CharSequence text : textList) {
                Log.d("text", text.toString());
                if (text.toString().length() > 0) {
                    openSendUI(event, text.toString());
                }
            }
        }
    }

    private void openSendUI(AccessibilityEvent event, String text) {
        String[] cc = text.split(":");
        if (cc[1].trim().equals("附近人")) {
            Intent intent = new Intent();
            ComponentName cmp = new ComponentName("com.tencent.mm", "com.tencent.mm.ui.LauncherUI");
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setComponent(cmp);
            context.startActivity(intent);
            openNext("发现");
            openDelay(1000,"附近的人");


        }


    }
    //延迟打开界面
    private void openDelay(final int delaytime, final String text) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(delaytime);
                } catch (InterruptedException mE) {
                    mE.printStackTrace();
                }
                openNext(text);
            }
        }).start();
    }
    private void delayOpenNext(final String text){
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                openNext(text);
            }
        }, 1000);
    }

    private void openNext(String str) {
        AccessibilityNodeInfo nodeInfo = accessibilityService.getRootInActiveWindow();
        if (nodeInfo == null) {
            Toast.makeText(context, "rootWindow为空", Toast.LENGTH_SHORT).show();
            return;
        }
        List<AccessibilityNodeInfo> list = nodeInfo.findAccessibilityNodeInfosByText(str);
        if (list != null && list.size() > 0) {
            list.get(list.size() - 1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
            list.get(list.size() - 1).getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
        } else {
            Toast.makeText(context, "找不到有效的节点", Toast.LENGTH_SHORT).show();
        }
    }
}
