package com.xatalk.assistant;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by Administrator on 2018/3/13 0013.
 */
@Entity
public class ReplyKeyBean {

    @Id
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private String name;
    private String key;
    private String msg;

    @Generated(hash = 1992346161)
    public ReplyKeyBean(String id, String name, String key, String msg) {
        this.id = id;
        this.name = name;
        this.key = key;
        this.msg = msg;
    }

    @Generated(hash = 1734619406)
    public ReplyKeyBean() {
    }
}
