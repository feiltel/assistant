package com.xatalk.assistant;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Administrator on 2018/3/14 0014.
 */

public class SharedPreferHelper {
    public static String getString(Context context, String fileName, String key) {

        SharedPreferences sp = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        return sp.getString(key, "");
    }

    public static void setString(Context context, String fileName, String key,
                                 String value) {
        SharedPreferences sp = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        sp.edit().putString(key, value).commit();
    }

    public static boolean getBoolean(Context context, String fileName, String key
    ) {

        SharedPreferences sp = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        return sp.getBoolean(key, false);
    }

    public static void setBoolean(Context context, String fileName, String key,
                                  boolean defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        sp.edit().putBoolean(key, defaultValue).commit();

    }

    public static int getInt(Context context, String fileName, String key) {
        SharedPreferences sp = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        return sp.getInt(key, 0);
    }

    public static void setInt(Context context, String fileName, String key,
                              int defaultValue) {
        SharedPreferences sp = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        sp.edit().putInt(key, defaultValue).commit();
    }

    public static void removeference(Context context, String fileName, String key) {
        SharedPreferences sp = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        sp.edit().remove(key).commit();
    }

    public static void clearference(Context context, String fileName) {
        SharedPreferences sp = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        sp.edit().clear().commit();
    }
}
