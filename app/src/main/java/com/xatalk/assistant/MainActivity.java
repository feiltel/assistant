package com.xatalk.assistant;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //保持屏幕长亮
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        SwitchCompat switchCompat = findViewById(R.id.switch1);
        switchCompat.setChecked(SharedPreferHelper.getBoolean(MainActivity.this, "setting","statue"));
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 SharedPreferHelper.setBoolean(MainActivity.this, "setting","statue", isChecked);
            }
        });

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddKeyWordDialog();
            }
        });
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] textS = new String[]{"你是", "第三", "大酒店", "渐渐地就", "大姐姐"};
                Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                startActivity(intent);
                Random random = new Random();
                Toast.makeText(MainActivity.this, "" + textS[(int) (random.nextFloat() * 10 / 2)], Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void showAddKeyWordDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("添加关键词");
        View view1 = LayoutInflater.from(this).inflate(R.layout.dialog_addkeyword_layout, null);
        final EditText et_key = view1.findViewById(R.id.et_key);
        final EditText et_content = view1.findViewById(R.id.et_content);
        builder.setView(view1);
        builder.setPositiveButton("保存", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ReplyKeyDataUtils dataUtils = new ReplyKeyDataUtils(getApplicationContext());
                ReplyKeyBean bean = new ReplyKeyBean();
                bean.setId(UUID.randomUUID().toString());
                bean.setKey(et_key.getText().toString());
                bean.setName("关键词一");
                bean.setMsg(et_content.getText().toString());
                dataUtils.insertData(bean);
            }
        }).show();
    }
}
